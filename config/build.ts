import * as fs from 'fs';
import * as path from 'path';
import * as uglify from 'uglify-es';
import * as dts from 'dts-bundle';

const getFiles = (dir: string, fileFilter: (file) => {}) => {
  let results = [];
  const files = fs.readdirSync(dir);
  for (const file of files) {
    const fullName = path.join(dir, file);
    if (fs.statSync(fullName).isDirectory()) {
      results = results.concat(getFiles(fullName, fileFilter));
    } else {
      if (fileFilter(file)) {
        results.push(fullName);
      }
    }
  }
  return results;
};

const jsFiles = getFiles('./dist', file => path.parse(file).ext === '.js');
for (const file of jsFiles) {
  const fileMinify = uglify.minify(fs.readFileSync(file, 'utf-8')).code;
  fs.writeFileSync(file, fileMinify);
}

fs.copyFileSync('./README.md', './dist/README.md');
const packageFile = JSON.parse(fs.readFileSync('./package.json', 'utf-8'));
fs.writeFileSync('./dist/package.json', JSON.stringify({
  name: '@telmoandrade/grpc-ts',
  version: packageFile.version,
  description: packageFile.description,
  author: packageFile.author,
  license: packageFile.license,
  keywords: packageFile.keywords,
  repository: packageFile.repository,
  bugs: packageFile.bugs,
  homepage: packageFile.homepage,
  peerDependencies: packageFile.dependencies,
  main: 'index.js',
  types: 'index.d.ts',
  bin: {
    'grpc-server': './bin/bin-server.js',
    'grpc-client': './bin/bin-client.js',
  },
}, null, 2));

const definitionFiles = getFiles('./dist', (file: string) => file.endsWith('.d.ts'));
dts.bundle({
  name: '@telmoandrade/grpc-ts',
  main: './dist/index.d.ts',
  out: './dist/index.temp.d.ts',
  headerPath: 'none',
  baseDir: './',
  outputAsModuleFolder: true,
  indent: '  ',
});

for (const definitionFile of definitionFiles) {
  fs.unlinkSync(definitionFile);
}
fs.renameSync('./dist/index.temp.d.ts', './dist/index.d.ts');
