import { loadSync } from '@grpc/proto-loader';
import { ChannelCredentials, loadPackageDefinition } from 'grpc';
import { ReplaySubject } from 'rxjs';

import { Logger } from '../utils/logger';
import { InterceptorOptions } from '../interfaces/interceptor';

export function GrpcClient<T>(options: {
  protoFile: string;
  host: string;
  creadential: ChannelCredentials;
  serviceName: string;
  methods: { [P in keyof T]: string };
}): PropertyDecorator {
  const { protoFile, serviceName, host, creadential } = options;

  const packageLoad = loadSync(protoFile);
  const propertyName = Object.getOwnPropertyNames(packageLoad)[0].split('.');
  propertyName.pop();
  const packageName = propertyName.join('.');
  propertyName.push(serviceName);
  let packageDefinition: any = loadPackageDefinition(packageLoad);
  while (propertyName.length > 0) {
    const item = propertyName.shift();
    packageDefinition = packageDefinition[item];
    if (!packageDefinition) break;
  }

  const createInterceptCallback = (intercept: Function, interceptorOptions: InterceptorOptions): (err) => void => {
    try {
      const callback = intercept(interceptorOptions);
      let call = false;
      return (err) => {
        try {
          if (call === false) {
            callback(err);
            call = true;
          }
        } catch (e) { console.error(e); }
      };
    } catch (e) { console.error(e); }
  };

  return (target: any, propertyKey: string | symbol): void => {
    const logger = new Logger();

    const log = (message: string) => {
      logger.log(`${logger.green('[gRPC-Client]')} ${(new Date()).toISOString()} ${message}`);
    };

    if (packageDefinition && packageDefinition.service) {
      target[propertyKey] = {};
      const { service } = packageDefinition;

      const protoMethods = Object.getOwnPropertyNames(service);
      const configMethods = Object.getOwnPropertyNames(options.methods)
        .map(m => ({ methodName: m, grpcName: options.methods[m] }));

      const methodNotExists = configMethods
        .filter(f => !protoMethods.some(s => s === f.grpcName))
        .map(m => m.grpcName);
      if (methodNotExists.length > 0) {
        log(`Configured methods '${logger.magenta(methodNotExists.join(','))}' that do not exist in the service '${serviceName}' not found in object '${target.constructor.name}'`);
      }

      const methodNotConfig = protoMethods
        .filter(f => !configMethods.some(s => s.grpcName === f))
        .map(m => m);
      if (methodNotConfig.length > 0) {
        log(`Methods '${logger.magenta(methodNotConfig.join(','))}' that were not configured in the service '${serviceName}' not found in object '${target.constructor.name}'`);
      }

      const intercept = target.intercept || (() => () => { });
      configMethods
        .filter(f => protoMethods.some(s => s === f.grpcName))
        .forEach(m => {
          const method = service[m.grpcName];
          const { requestStream, responseStream } = method;

          if (requestStream === false && responseStream === false) {
            // Unary Unary
            target[propertyKey][m.methodName] = (message, metadata) => {
              const callback = createInterceptCallback(intercept, { packageName, serviceName, methodName: m.grpcName });
              return new Promise((resolve, reject) => {
                const grpc = new packageDefinition(host, creadential, {});
                grpc[m.grpcName](message, metadata, (err, result) => {
                  if (err) { reject(err); callback(err); } else { resolve(result); callback(null); }
                });
              });
            };
          } else if (requestStream === true && responseStream === false) {
            // Stream Unary
            target[propertyKey][m.methodName] = (message, metadata) => {
              const callback = createInterceptCallback(intercept, { packageName, serviceName, methodName: m.grpcName });
              return new Promise((resolve, reject) => {
                const grpc = new packageDefinition(host, creadential, {});
                const call = grpc[m.grpcName](metadata, (err, result) => {
                  if (err) { reject(err); callback(err); } else { resolve(result); callback(null); }
                });
                message.subscribe(
                  next => call.write(next),
                  err => { call.cancel(); reject(err); callback(err); },
                  () => call.end(),
                );
              });
            };
          } else if (requestStream === false && responseStream === true) {
            // Unary Stream
            target[propertyKey][m.methodName] = (message, metadata) => {
              const callback = createInterceptCallback(intercept, { packageName, serviceName, methodName: m.grpcName });
              const grpc = new packageDefinition(host, creadential, {});
              const subject = new ReplaySubject();
              const call = grpc[m.grpcName](message, metadata);
              call.on('data', next => { if (!subject.isStopped) { subject.next(next); } else { call.cancel(); } });
              call.on('error', err => { callback(err); if (!subject.isStopped) { subject.error(err); } });
              call.on('end', () => { callback(null); if (!subject.isStopped) { subject.complete(); } });
              return subject;
            };
          } else {
            // Stream Stream
            target[propertyKey][m.methodName] = (message, metadata) => {
              const callback = createInterceptCallback(intercept, { packageName, serviceName, methodName: m.grpcName });
              const grpc = new packageDefinition(host, creadential, {});
              const subject = new ReplaySubject();
              const call = grpc[m.grpcName](metadata);
              call.on('data', next => { if (!subject.isStopped) { subject.next(next); } else { call.cancel(); } });
              call.on('error', err => { callback(err); if (!subject.isStopped) { subject.error(err); } });
              call.on('end', () => { callback(null); if (!subject.isStopped) { subject.complete(); } });
              message.subscribe(
                next => call.write(next),
                err => { call.cancel(); if (!subject.isStopped) { subject.error(err); callback(err); } },
                () => call.end(),
              );
              return subject;
            };
          }
        });
    } else {
      log(`Service '${logger.red(serviceName)}' not found in object '${target.constructor.name}'`);
    }
  };
}
