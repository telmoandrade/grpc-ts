import { GRPC_METHOD, GRPC_INJECTABLE } from './const';
import { Interceptor } from '../interfaces/interceptor';
import { CanActivate } from '../interfaces/can-activate';
import { GrpcMethodDefinition } from '../interfaces/grpc-method-definition';

export interface GrpcServiceOptions {
  serviceName?: string;
  guard?: new (...args: any) => CanActivate;
  interceptor?: new (...args: any) => Interceptor;
}

export function GrpcService(options?: GrpcServiceOptions): ClassDecorator {
  return (target: Function) => {
    const { serviceName } = {
      serviceName: target.name,
      ...options,
    } as GrpcServiceOptions;

    const { guard, interceptor } = options || {} as GrpcServiceOptions;

    const methods: GrpcMethodDefinition[] = Reflect.getMetadata(GRPC_METHOD, target) || [];
    methods.forEach(f => {
      f.serviceName = f.serviceName || serviceName;
      f.guard = f.guard || guard;
      f.interceptor = f.interceptor || interceptor;
    });
    Reflect.defineMetadata(GRPC_METHOD, methods, target);
    Reflect.defineMetadata(GRPC_INJECTABLE, 'injectable', target);
  };
}
