import { GRPC_SERVER } from './const';

export interface GrpcServerConfig {
  modules: Function[];
  services?: Function[];
}

export function GrpcServer(config: GrpcServerConfig): ClassDecorator {
  return (target: Function) => {
    config.modules = config.modules || [];
    config.services = config.services || [];
    Reflect.defineMetadata(GRPC_SERVER, config, target);
  };
}
