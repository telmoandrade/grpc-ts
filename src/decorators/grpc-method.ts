import { GRPC_METHOD } from './const';
import { GrpcMethodDefinition } from '../interfaces/grpc-method-definition';
import { CanActivate } from '../interfaces/can-activate';
import { Interceptor } from '../interfaces/interceptor';

export interface GrpcMethodOptions {
  serviceName?: string;
  methodName?: string;
  classValidation?: Function;
  guard?: new (...args: any) => CanActivate;
  interceptor?: new (...args: any) => Interceptor;
}

export function GrpcMethod(options?: GrpcMethodOptions): MethodDecorator {
  return (target: Object, propertyKey: string, descriptor: PropertyDescriptor): PropertyDescriptor | void => {
    const capitalizeFirstLetter = (str: string) => str.charAt(0).toUpperCase() + str.slice(1);

    const { serviceName, methodName, guard, interceptor, classValidation } = {
      methodName: capitalizeFirstLetter(propertyKey),
      ...options,
    } as GrpcMethodOptions;

    const grpcMethodDefinition: GrpcMethodDefinition = {
      propertyKey,
      descriptor,
      packageName: null,
      serviceName,
      methodName,
      classValidation,
      guard,
      interceptor,
      instance: null,
    };

    Reflect.defineMetadata(GRPC_METHOD, [...Reflect.getMetadata(GRPC_METHOD, target.constructor) || [], grpcMethodDefinition], target.constructor);
    return descriptor;
  };
}
