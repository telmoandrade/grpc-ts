import { GRPC_MODULE } from './const';

export interface GrpcModuleConfig {
  protoFile: string;
  services: Function[];
}

export function GrpcModule(config: GrpcModuleConfig): ClassDecorator {
  return (target: Function) => {
    config.services = config.services || [];
    Reflect.defineMetadata(GRPC_MODULE, config, target);
  };
}
