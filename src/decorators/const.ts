export const GRPC_SERVER = 'grpc:server';
export const GRPC_MODULE = 'grpc:module';
export const GRPC_METHOD = 'grpc:method';
export const GRPC_INJECTABLE = 'grpc:injectable';
export const GRPC_INSTANCES = 'grpc:instances';
