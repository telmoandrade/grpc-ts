import { GRPC_INJECTABLE } from './const';

export function Injectable(): ClassDecorator {
  return (target: Function) => {
    Reflect.defineMetadata(GRPC_INJECTABLE, 'injectable', target);
  };
}
