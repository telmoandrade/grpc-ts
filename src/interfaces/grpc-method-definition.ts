import { CanActivate } from './can-activate';
import { Interceptor } from './interceptor';
import { GrpcError } from '../utils/grpc-error';

export interface GrpcMethodDefinition {
  propertyKey: string;
  descriptor: PropertyDescriptor;
  packageName: string;
  serviceName: string;
  methodName: string;
  instance: Function;
  guard?: new (...args: any) => CanActivate;
  guardInstance?: CanActivate;
  interceptor?: new (...args: any) => Interceptor;
  interceptorInstance?: Interceptor;
  classValidation: any;
  pipeError?: (err: GrpcError) => GrpcError;
}
