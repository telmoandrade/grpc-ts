import { Metadata } from 'grpc';

export interface CanActivate {
  canActivate(metadata: Metadata): any | Promise<any>;
}
