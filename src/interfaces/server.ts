import { EventEmitter } from 'events';

import { Credentials } from './credentials';
import { CanActivate } from './can-activate';
import { Interceptor } from './interceptor';
import { GrpcError } from '../utils/grpc-error';

export interface Server extends EventEmitter {
  useGlobalGuard(guardActivate: new (...args: any) => CanActivate): void;
  useGlobalInterceptor(interceptor: new (...args: any) => Interceptor): void;
  listenAsync(address: string, serverCredentials: Credentials): Promise<number>;
  pipeError(pipe: (err: GrpcError) => GrpcError): void;
}
