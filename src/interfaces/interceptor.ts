import { GrpcError } from '../utils/grpc-error';
import { MetadataValue } from 'grpc';

export interface InterceptorData {
  request: any[];
  response: any[];
  metadata: { [key: string]: MetadataValue };
  cancelled: boolean;
}

export interface InterceptorOptions {
  packageName: string;
  serviceName: string;
  methodName: string;
}

export interface Interceptor {
  intercept(options: InterceptorOptions): (err: GrpcError, data: InterceptorData) => void;
}
