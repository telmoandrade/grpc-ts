import 'reflect-metadata';
import { Server as _Server, ServerCredentials, loadPackageDefinition, ServerUnaryCall, ServerWriteableStream, ServerReadableStream, status, setLogger, KeyCertPair, ServerDuplexStream } from 'grpc';
import { loadSync } from '@grpc/proto-loader';
import { ReplaySubject } from 'rxjs';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { EventEmitter } from 'events';

import { GRPC_SERVER, GRPC_MODULE, GRPC_METHOD, GRPC_INSTANCES, GRPC_INJECTABLE } from './decorators/const';
import { Server } from './interfaces/server';
import { CanActivate } from './interfaces/can-activate';
import { Interceptor, InterceptorData } from './interfaces/interceptor';
import { Credentials } from './interfaces/credentials';
import { GrpcMethodDefinition } from './interfaces/grpc-method-definition';
import { GrpcError } from './utils/grpc-error';
import { UniqueObjectArray } from './utils/unique-object.array';
import { Logger } from './utils/logger';
import { GrpcServerConfig } from './decorators/grpc-server';
import { GrpcModuleConfig } from './decorators/grpc-module';

interface InvalidMetadataMessage {
  type: 'error' | 'warn';
  message?: 'configureDecorator' | 'multipleServices' | 'serviceNotFound' | 'selfService' | 'serviceNotFound' | 'newInstance' | 'newInstanceService' | 'classValidationNotFound';
  values: { [index: string]: any };
}

export interface GrpcFactoryOptions {
  grpcMaxSendMessageLength?: number;
  grpcMaxReceiveMessageLength?: number;
}

export class GrpcFactory {
  private log: (message: string, target: string, milliseconds?: number) => void;
  private logger: Logger;
  private options: GrpcFactoryOptions;
  private grpcServer: _Server;
  private server: Server;

  private constructor(options?: GrpcFactoryOptions) {
    this.options = { ...options };

    this.logger = new Logger();
    this.log = (message: string, target: string, milliseconds?: number) => {
      const m = milliseconds !== undefined ? this.logger.yellow(`+${milliseconds}ms`) : '';
      this.logger.log(`${this.logger.green('[gRPC]')}  ${(new Date().toISOString())}  ${this.logger.yellow(`[${target}]`)} ${message} ${m}`);
    };

    const serverOptions = {};

    if (this.options.grpcMaxSendMessageLength) {
      serverOptions['grpc.max_send_message_length'] = this.options.grpcMaxSendMessageLength;
    }
    if (this.options.grpcMaxReceiveMessageLength) {
      serverOptions['grpc.max_receive_message_length'] = this.options.grpcMaxReceiveMessageLength;
    }

    this.grpcServer = new _Server(serverOptions);
    this.server = new EventEmitter() as Server;
  }

  static createInsecure(): ServerCredentials {
    return ServerCredentials.createInsecure();
  }

  static createSsl(rootCerts: Buffer | null, keyCertPairs: KeyCertPair[], checkClientCertificate?: boolean): ServerCredentials {
    return ServerCredentials.createSsl(rootCerts, keyCertPairs, checkClientCertificate);
  }

  static async create(serverModule: Function, options?: GrpcFactoryOptions): Promise<Server> {
    const grpcFactory = new GrpcFactory(options);
    const metadata = Reflect.getMetadata(GRPC_SERVER, serverModule) as GrpcServerConfig;

    if (!metadata) {
      throw new Error(`Use @GrpcServer() in class ${serverModule.name}`);
    }
    const { modules, services } = metadata;
    if (modules.length === 0) {
      throw new Error(`@GrpcServer() in class ${serverModule.name} to contain at least 1 module`);
    }

    setLogger({ error: () => { } } as Console);
    await grpcFactory.validateMetadata(modules, services);
    await grpcFactory.createInstances(modules, services);
    await grpcFactory.addServices(modules);

    grpcFactory.server.listenAsync = async (address: string, serverCredentials: Credentials): Promise<number> => {
      return new Promise<number>((resolve, reject) => {
        const milliseconds = (new Date()).getTime();
        grpcFactory.grpcServer.bindAsync(address, serverCredentials, (errBind: Error, port: number) => {
          if (errBind) {
            reject(errBind);
          } else {
            try {
              grpcFactory.grpcServer.start();
              grpcFactory.log(`Server on ${address}`, 'Microservice', (new Date()).getTime() - milliseconds);
              resolve(port);
            } catch (errStart) {
              reject(errStart);
            }
          }
        });
      });
    };
    grpcFactory.server.useGlobalGuard = (guardActivate: new (...args: any) => CanActivate): void => {
      for (const module of modules) {
        const methods: GrpcMethodDefinition[] = Reflect.getMetadata(GRPC_METHOD, module) || [];
        const moduleInstances: Map<any, any> = Reflect.getMetadata(GRPC_INSTANCES, module);
        const guardInstance = moduleInstances.get(guardActivate) as CanActivate;
        if (!guardInstance) {
          grpcFactory.log(`Instance not found ${grpcFactory.logger.red(guardActivate.name)}`, 'Global Guard');
          process.exit(1);
        }
        methods.forEach(f => {
          f.guardInstance = f.guardInstance || guardInstance;
        });
      }
    };
    grpcFactory.server.useGlobalInterceptor = (interceptor: new (...args: any) => Interceptor): void => {
      for (const module of modules) {
        const methods: GrpcMethodDefinition[] = Reflect.getMetadata(GRPC_METHOD, module) || [];
        const moduleInstances: Map<any, any> = Reflect.getMetadata(GRPC_INSTANCES, module);
        const interceptorInstance = moduleInstances.get(interceptor) as Interceptor;
        if (!interceptorInstance) {
          grpcFactory.log(`Instance not found ${grpcFactory.logger.red(interceptor.name)}`, 'Global Interceptor');
          process.exit(1);
        }
        methods.forEach(f => {
          f.interceptorInstance = f.interceptorInstance || interceptorInstance;
        });
      }
    };

    grpcFactory.server.pipeError = (pipe: (err: GrpcError) => GrpcError): void => {
      for (const module of modules) {
        const methods: GrpcMethodDefinition[] = Reflect.getMetadata(GRPC_METHOD, module) || [];
        methods.forEach(f => { f.pipeError = pipe; });
      }
    };

    return grpcFactory.server;
  }

  private async validateMetadata(modules: Function[], services: Function[]): Promise<void> {
    const milliseconds = (new Date()).getTime();
    const uniqueObjectArray = new UniqueObjectArray<InvalidMetadataMessage>();

    const serviceResults = await this.validateMetadataServices(services);
    serviceResults.forEach(f => {
      uniqueObjectArray.push(f);
    });

    const moduleResults = await this.validateMetadataModules(modules, services);
    moduleResults.forEach(f => {
      uniqueObjectArray.push(f);
    });

    const invalidMetadataMessage = uniqueObjectArray.get();
    invalidMetadataMessage.forEach(f => {
      let message = '';
      const { className, serviceName, decoratorName, targetName, params, methodName } = f.values;
      switch (f.message) {
        case 'configureDecorator':
          message = `Use the decorator ${this.logger.red(decoratorName)} in class ${this.logger.red(targetName)}`;
          break;
        case 'serviceNotFound':
          const serviceNotFoundParam = params.map(x => !x.found ? this.logger.red(`${x.paramName} ?`) : x.paramName).join(', ');
          message = `Instance not found to start class ${this.logger.magenta(className)}(${serviceNotFoundParam})`;
          break;
        case 'multipleServices':
          message = `It is not allowed to insert multiple instances of ${this.logger.red(serviceName)} in constructur of class ${this.logger.red(className)}`;
          break;
        case 'newInstance':
          message = `In decorator @GrpcModule() the module ${this.logger.blue(className)} this by injecting a new instance of class ${this.logger.blue(serviceName)}`;
          break;
        case 'newInstanceService':
          message = `The module ${this.logger.blue(className)} this by injecting a new instance of class ${this.logger.blue(serviceName)}${this.logger.blue('(')}${this.logger.cyan(params)}${this.logger.blue(')')} because of its dependency`;
          break;
        case 'selfService':
          message = `Self service is not allowed in the constructor in class ${this.logger.red(className)}`;
          break;
        case 'serviceNotFound':
          message = `The ${this.logger.red(className)} module must contain at least 1 service defined in the @GrpcModule()`;
          break;
        case 'classValidationNotFound':
          message = `It is not possible to use the class-validator in param ${this.logger.red(params)} in method ${this.logger.red(methodName)} of class ${this.logger.red(className)}`;
          break;
      }
      this.log(message, 'Decorators');
    });
    if (invalidMetadataMessage.some(s => s.type === 'error')) {
      throw new Error('Dependency Service Error');
    }

    this.log('Validated decorators', 'Decorators', (new Date()).getTime() - milliseconds);
  }

  private async validateMetadataServices(services: Function[]): Promise<InvalidMetadataMessage[]> {
    const uniqueObjectArray = new UniqueObjectArray<InvalidMetadataMessage>();

    for (const service of services) {
      const paramTypes: Function[] = Reflect.getMetadata('design:paramtypes', service) || [];
      if (paramTypes.some(f => f === service)) {
        uniqueObjectArray.push({ type: 'error', message: 'selfService', values: { className: service.name } });
      }

      const metadata = Reflect.getMetadata(GRPC_INJECTABLE, service);
      if (!metadata) {
        uniqueObjectArray.push({ type: 'error', message: 'configureDecorator', values: { decoratorName: '@Injectable() or @GrpcService()', targetName: service.name } });
      }

      paramTypes
        .reduce((acc, r) => {
          if (paramTypes.filter(f => f === r).length !== 1 && !acc.some(s => s === r)) {
            acc = [...acc, r];
          }
          return acc;
        }, [])
        .forEach(f => {
          const { name } = f || { name: 'void' };
          uniqueObjectArray.push({ type: 'error', message: 'multipleServices', values: { className: service.name, serviceName: name } });
        });

      const filter = paramTypes.filter(f => f !== service && !services.some(i => i === f));
      if (filter.length > 0) {
        const params = paramTypes.map(m => {
          const { name } = m || { name: 'void' };
          return {
            paramName: name,
            found: !filter.some(s => s === m),
          };
        });
        uniqueObjectArray.push({ type: 'error', message: 'serviceNotFound', values: { className: service.name, params } });
      }

      const methods: GrpcMethodDefinition[] = Reflect.getMetadata(GRPC_METHOD, service);
      if (methods) {
        methods.forEach(f => {
          if (f.classValidation && (f.classValidation === String || f.classValidation === Boolean || f.classValidation === Number || f.classValidation === Object || f.classValidation === Array)) {
            uniqueObjectArray.push({ type: 'error', message: 'classValidationNotFound', values: { className: service.name, methodName: f.methodName, params: f.classValidation.name } });
          }
        });
      }
    }

    return uniqueObjectArray.get();
  }

  private async validateMetadataModules(modules: Function[], services: Function[]): Promise<InvalidMetadataMessage[]> {
    const uniqueObjectArray = new UniqueObjectArray<InvalidMetadataMessage>();

    for (const module of modules) {
      const metadata = Reflect.getMetadata(GRPC_MODULE, module) as GrpcModuleConfig;
      if (!metadata) {
        uniqueObjectArray.push({ type: 'error', message: 'configureDecorator', values: { decoratorName: '@GrpcModule()', targetName: module.name } });
      } else {
        metadata.services
          .filter(f => {
            return services.some(i => i === f);
          })
          .forEach(i => {
            uniqueObjectArray.push({ type: 'warn', message: 'newInstance', values: { className: module.name, serviceName: i.name } });
          });

        services
          .filter(f => {
            const params = (Reflect.getMetadata('design:paramtypes', f) || []);
            return !metadata.services.some(s => s === f) && params.some(p => metadata.services.some(s => s === p));
          })
          .forEach(i => {
            const params = (Reflect.getMetadata('design:paramtypes', i) || []);
            const filter = params.filter(f => metadata.services.some(s => s === f));
            uniqueObjectArray.push({ type: 'warn', message: 'newInstanceService', values: { className: module.name, seviceName: i.name, params: filter.map(m => m.name).join(',') } });
          });

        if (metadata.services.length === 0) {
          uniqueObjectArray.push({ type: 'error', message: 'serviceNotFound', values: { className: module.name } });
        }

        const servicesResults = await this.validateMetadataServices([...metadata.services, ...services.filter(f => !metadata.services.some(s => s === f))]);
        servicesResults.forEach(f => {
          uniqueObjectArray.push(f);
        });
      }
    }

    return uniqueObjectArray.get();
  }

  private async createInstances(modules: Function[], sevices: any[]): Promise<void> {
    const sharedInstances: Map<any, any> = await this.createInstancesBuild(sevices);

    for (const module of modules) {
      const metadata = Reflect.getMetadata(GRPC_MODULE, module) as GrpcModuleConfig;

      const reuseInstances: Map<any, any> = new Map();
      sevices.filter(f => {
        const params = (Reflect.getMetadata('design:paramtypes', f) || []);
        return !metadata.services.some(s => s === f) && !params.some(p => metadata.services.some(s => s === p));
      }).forEach(f => {
        reuseInstances.set(f, sharedInstances.get(f));
      });
      const rebuildServices = sevices.filter(f => {
        const params = (Reflect.getMetadata('design:paramtypes', f) || []);
        return metadata.services.some(s => s === f) || params.some(p => metadata.services.some(s => s === p));
      });

      const moduleInstances: Map<any, any> = await this.createInstancesBuild([...rebuildServices, ...metadata.services], reuseInstances);
      Reflect.defineMetadata(GRPC_INSTANCES, moduleInstances, module);
      metadata.services.forEach(service => {
        const moduleMethods: GrpcMethodDefinition[] = Reflect.getMetadata(GRPC_METHOD, module) || [];
        const serviceMethods: GrpcMethodDefinition[] = Reflect.getMetadata(GRPC_METHOD, service) || [];
        serviceMethods.forEach(f => {
          f.instance = moduleInstances.get(service);
          f.guardInstance = moduleInstances.get(f.guard) as CanActivate;
          f.interceptorInstance = moduleInstances.get(f.interceptor) as Interceptor;
          f.pipeError = (err: GrpcError) => err;
        });
        Reflect.defineMetadata(GRPC_METHOD, [...moduleMethods, ...serviceMethods], module);
      });
    }
  }

  private async createInstancesBuild(targets: any, instances?: Map<any, any>): Promise<Map<any, any>> {
    instances = instances || new Map();

    targets = targets.map(x => x);
    while (targets.length > 0) {
      const init = targets.length;
      for (const target of targets) {
        const args = [];
        const params = (Reflect.getMetadata('design:paramtypes', target) || []);
        for (const param of params) {
          const instance = instances.get(param);
          if (instance) {
            args.push(instance);
          }
        }
        if (args.length === params.length) {
          const instance = new target(...args);
          instances.set(target, instance);
          targets.splice(targets.indexOf(target), 1);
        }
      }
      if (init === targets.length) {
        const message = targets.map(target => {
          const args = [];
          const params = (Reflect.getMetadata('design:paramtypes', target) || []);
          for (const param of params) {
            const instance = instances.get(param);
            if (instance) {
              args.push(param.name);
            } else {
              args.push('?');
            }
          }

          return `new ${target.name}(${args.join(',')})`;
        });
        throw new Error('Dependency instance not found\n' + message.join('\n'));
      }
    }

    return instances;
  }

  private async addServices(modules: Function[]): Promise<void> {
    const milliseconds = (new Date()).getTime();
    for (const module of modules) {
      const metadataModule: GrpcModuleConfig = Reflect.getMetadata(GRPC_MODULE, module);
      const metadataMethods: GrpcMethodDefinition[] = Reflect.getMetadata(GRPC_METHOD, module) || [];

      const packageLoad = loadSync(metadataModule.protoFile);
      const propertyName = Object.getOwnPropertyNames(packageLoad)[0].split('.');
      propertyName.pop();
      let packageObject: any = loadPackageDefinition(packageLoad);
      const packageName = [];
      while (propertyName.length > 0) {
        const item = propertyName.shift();
        packageName.push(item);
        packageObject = packageObject[item];
        if (!packageObject) break;
      }

      for (const key of Object.getOwnPropertyNames(packageObject)) {
        const objectDefinition = packageObject[key];
        if (objectDefinition && objectDefinition.service) {
          const serviceName = key;
          const serviceDefinition = objectDefinition;
          const serviceImplementation = {};
          for (const methodName of Object.getOwnPropertyNames(serviceDefinition.service)) {
            const fullName = `${module.name}/${serviceName}.${methodName}`;
            const methodDefinition = serviceDefinition.service[methodName];
            const metadataMethod = metadataMethods.find(x => x.serviceName === serviceName && x.methodName === methodName);
            if (metadataMethod) {
              metadataMethod.packageName = packageName.join('.');
              const methodValue = await this.addServicesCreateMethod(methodDefinition, metadataMethod);
              if (methodValue) {
                serviceImplementation[methodName] = methodValue;
                this.log(`Mapped method of gRPC ${this.logger.green(fullName)}`, 'Mapping gRPC methods');
              }
            } else {
              this.log(`Unmapped method of gRPC ${this.logger.magenta(fullName)}`, 'Mapping gRPC methods');
            }
          }
          this.grpcServer.addService(serviceDefinition.service, serviceImplementation);
        }
      }
    }

    this.log('Mapped methods', 'Mapping gRPC methods', (new Date()).getTime() - milliseconds);
  }

  private async addServicesCreateMethod(methodDefinition, metadataMethod: GrpcMethodDefinition): Promise<Function> {
    const grpcName = `${metadataMethod.serviceName}.${metadataMethod.methodName}`;

    const returnType = Reflect.getMetadata('design:returntype', metadataMethod.instance, metadataMethod.propertyKey);
    const paramTypes = Reflect.getMetadata('design:paramtypes', metadataMethod.instance, metadataMethod.propertyKey);

    let fail = false;
    if (methodDefinition.responseStream === false && returnType !== Promise) {
      this.log(`The implementation of method ${this.logger.red(grpcName)} should return a Promise`, 'Mapping gRPC methods');
      fail = true;
    }
    if (methodDefinition.responseStream === true && returnType.name !== 'ReplaySubject') {
      this.log(`The implementation of method ${this.logger.red(grpcName)} should return a ReplaySubject`, 'Mapping gRPC methods');
      fail = true;
    }
    if (methodDefinition.requestStream === false && paramTypes.length > 0 && paramTypes[0] && paramTypes[0].name === 'ReplaySubject') {
      this.log(`The implementation of method ${this.logger.red(grpcName)} can not have the first parameter a ReplaySubject`, 'Mapping gRPC methods');
      fail = true;
    }
    if (methodDefinition.requestStream === true && (paramTypes.length === 0 || !paramTypes[0] || paramTypes[0].name !== 'ReplaySubject')) {
      this.log(`The implementation of method ${this.logger.red(grpcName)} must have as the first paramenter a ReplaySubject`, 'Mapping gRPC methods');
      fail = true;
    }

    if (!fail) {
      if (methodDefinition.requestStream === false) {
        if (methodDefinition.responseStream === false) {
          return await this.addServicesCreateUnaryUnaryMethod(metadataMethod);
        } else {
          return await this.addServicesCreateUnaryStreamMethod(metadataMethod);
        }
      } else {
        if (methodDefinition.responseStream === false) {
          return await this.addServicesCreateStreamUnaryMethod(metadataMethod);
        } else {
          return await this.addServicesCreateStreamStreamMethod(metadataMethod);
        }
      }
    }
  }

  private async addServicesCreateUnaryUnaryMethod(metadataMethod: GrpcMethodDefinition) {
    const { instance, descriptor, classValidation } = metadataMethod;
    const originalValue = descriptor.value;

    return async (call: ServerUnaryCall<any>, callback) => {
      const { guardInstance } = metadataMethod;
      const interceptData: InterceptorData = { request: [], response: [], metadata: call.metadata.getMap(), cancelled: false };
      const interceptCallback = this.createInterceptCallback(metadataMethod);
      try {
        const guardValue = guardInstance && await guardInstance.canActivate(call.metadata);
        interceptData.request.push(call.request);
        if (classValidation && classValidation !== Object) {
          const entity = plainToClass(classValidation, call.request);
          const invalidArgument = await validate(entity);
          if (invalidArgument.length > 0) {
            throw new GrpcError('INVALID_ARGUMENT', status.INVALID_ARGUMENT, invalidArgument);
          }
        }

        const value = await originalValue.apply(instance, [call.request, call.metadata, guardValue]);
        interceptData.response.push(value);
        if (!call.cancelled) {
          callback(null, value);
        }
        interceptCallback(null, { ...interceptData, cancelled: call.cancelled });
      } catch (err) {
        err = this.buildError(err);
        err = metadataMethod.pipeError ? metadataMethod.pipeError(err) : err;
        if (!call.cancelled) {
          callback(err, null);
        }
        interceptCallback(err, { ...interceptData, cancelled: call.cancelled });
      }
    };
  }

  private async addServicesCreateUnaryStreamMethod(metadataMethod: GrpcMethodDefinition) {
    const { instance, descriptor, classValidation } = metadataMethod;
    const originalValue = descriptor.value;

    return async (call: ServerWriteableStream<any>) => {
      const { guardInstance } = metadataMethod;
      const interceptData: InterceptorData = { request: [], response: [], metadata: call.metadata.getMap(), cancelled: false };
      const interceptCallback = this.createInterceptCallback(metadataMethod);
      try {
        const guardValue = guardInstance && await guardInstance.canActivate(call.metadata);
        interceptData.request.push(call.request);
        if (classValidation && classValidation !== Object) {
          const entity = plainToClass(classValidation, call.request);
          const invalidArgument = await validate(entity);
          if (invalidArgument.length > 0) {
            throw new GrpcError('INVALID_ARGUMENT', status.INVALID_ARGUMENT, invalidArgument);
          }
        }

        const subject = originalValue.apply(instance, [call.request, call.metadata, guardValue]) as ReplaySubject<any>;
        subject.subscribe(
          next => {
            if (!call.cancelled) {
              interceptData.response.push(next);
              call.write(next);
            } else {
              if (!subject.isStopped) {
                subject.error(new GrpcError('CANCELLED', status.CANCELLED));
              }
            }
          },
          err => {
            err = this.buildError(err);
            err = metadataMethod.pipeError ? metadataMethod.pipeError(err) : err;
            if (!call.cancelled) {
              call.emit('error', err);
            }
            interceptCallback(err, { ...interceptData, cancelled: call.cancelled });
          },
          () => {
            if (!call.cancelled) {
              call.end();
            }
            interceptCallback(null, { ...interceptData, cancelled: call.cancelled });
          },
        );
      } catch (err) {
        err = this.buildError(err);
        err = metadataMethod.pipeError ? metadataMethod.pipeError(err) : err;
        if (!call.cancelled) {
          call.emit('error', err);
        }
        interceptCallback(err, { ...interceptData, cancelled: call.cancelled });
      }
    };
  }

  private async addServicesCreateStreamUnaryMethod(metadataMethod: GrpcMethodDefinition) {
    const { instance, descriptor, classValidation } = metadataMethod;
    const originalValue = descriptor.value;

    return async (call: ServerReadableStream<any>, callback) => {
      const { guardInstance } = metadataMethod;
      const interceptData: InterceptorData = { request: [], response: [], metadata: call.metadata.getMap(), cancelled: false };
      const interceptCallback = this.createInterceptCallback(metadataMethod);
      const subject = new ReplaySubject<any>();
      try {
        const guardValue = guardInstance && await guardInstance.canActivate(call.metadata);
        call.on('data', async (next) => {
          interceptData.request.push(next);
          if (classValidation && classValidation !== Object) {
            const entity = plainToClass(classValidation, next);
            const invalidArgument = await validate(entity);
            if (invalidArgument.length > 0) {
              let err = new GrpcError('INVALID_ARGUMENT', status.INVALID_ARGUMENT, invalidArgument);
              err = metadataMethod.pipeError ? metadataMethod.pipeError(err) : err;
              if (!call.cancelled) {
                callback(err, null);
              }
              if (!subject.isStopped) {
                subject.error(err);
              }
            }
          }
          if (!subject.isStopped) {
            subject.next(next);
          }
        });
        call.on('end', () => {
          if (!subject.isStopped) {
            subject.complete();
          }
        });

        const value = await originalValue.apply(instance, [subject, call.metadata, guardValue]);
        interceptData.response.push(value);
        if (!call.cancelled) {
          callback(null, value);
        }
        interceptCallback(null, { ...interceptData, cancelled: call.cancelled });
      } catch (err) {
        err = this.buildError(err);
        err = metadataMethod.pipeError ? metadataMethod.pipeError(err) : err;
        if (!call.cancelled) {
          callback(err, null);
        }
        if (!subject.isStopped) {
          subject.error(err);
        }
        interceptCallback(err, { ...interceptData, cancelled: call.cancelled });
      }
    };
  }

  private async addServicesCreateStreamStreamMethod(metadataMethod: GrpcMethodDefinition) {
    const { instance, descriptor, classValidation } = metadataMethod;
    const originalValue = descriptor.value;

    return async (call: ServerDuplexStream<any, any>) => {
      const { guardInstance } = metadataMethod;
      const interceptData: InterceptorData = { request: [], response: [], metadata: call.metadata.getMap(), cancelled: false };
      const interceptCallback = this.createInterceptCallback(metadataMethod);
      const subjectRequest = new ReplaySubject<any>();
      try {
        const guardValue = guardInstance && await guardInstance.canActivate(call.metadata);
        call.on('data', async (next) => {
          interceptData.request.push(next);
          if (classValidation && classValidation !== Object) {
            const entity = plainToClass(classValidation, next);
            const invalidArgument = await validate(entity);
            if (invalidArgument.length > 0) {
              let err = new GrpcError('INVALID_ARGUMENT', status.INVALID_ARGUMENT, invalidArgument);
              err = metadataMethod.pipeError ? metadataMethod.pipeError(err) : err;
              if (!call.cancelled) {
                call.emit('error', err);
              }
              if (!subjectRequest.isStopped) {
                subjectRequest.error(err);
              }
            }
          }
          if (!subjectRequest.isStopped) {
            subjectRequest.next(next);
          }
        });
        call.on('end', () => {
          if (!subjectRequest.isStopped) {
            subjectRequest.complete();
          }
        });

        const subjectResponse = originalValue.apply(instance, [subjectRequest, call.metadata, guardValue]) as ReplaySubject<any>;
        subjectResponse.subscribe(
          next => {
            if (!call.cancelled) {
              interceptData.response.push(next);
              call.write(next);
            } else {
              if (!subjectResponse.isStopped) {
                subjectResponse.error(new GrpcError('CANCELLED', status.CANCELLED));
              }
            }
          },
          err => {
            err = this.buildError(err);
            err = metadataMethod.pipeError ? metadataMethod.pipeError(err) : err;
            if (!call.cancelled) {
              call.emit('error', err);
            }
            interceptCallback(err, { ...interceptData, cancelled: call.cancelled });
          },
          () => {
            if (!call.cancelled) {
              call.end();
            }
            interceptCallback(null, { ...interceptData, cancelled: call.cancelled });
          },
        );
      } catch (err) {
        err = this.buildError(err);
        err = metadataMethod.pipeError ? metadataMethod.pipeError(err) : err;
        if (!subjectRequest.isStopped) {
          subjectRequest.error(err);
        }
        if (!call.cancelled) {
          call.emit('error', err);
        }
        interceptCallback(err, { ...interceptData, cancelled: call.cancelled });
      }
    };
  }

  private createInterceptCallback(metadataMethod: GrpcMethodDefinition): (err: GrpcError, data: InterceptorData) => void {
    const { interceptorInstance, methodName, serviceName, packageName } = metadataMethod;
    try {
      const callback = interceptorInstance && interceptorInstance.intercept({ methodName, serviceName, packageName });
      let call = false;
      return (err: GrpcError, data: InterceptorData) => {
        try {
          if (call === false) {
            callback(err, data);
            call = true;
          }
        } catch (errCallback) {
          this.server.emit('error-interceptor', errCallback);
        }
      };
    } catch (errIntercept) {
      this.server.emit('error-interceptor', errIntercept);
      return (err: GrpcError, data: InterceptorData) => { };
    }
  }

  private buildError(err: Error): GrpcError {
    if (err instanceof GrpcError) {
      return err;
    }

    const grpcErr = new GrpcError(err.message, status.INTERNAL);
    grpcErr.stack = err.stack;
    grpcErr.innerError = err;
    return grpcErr;
  }
}
