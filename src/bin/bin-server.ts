#!/usr/bin/env node
import { existsSync, mkdirSync, writeFileSync } from 'fs';
import { join } from 'path';

import { Bin } from './bin';

class BinServer extends Bin {
  commandName = 'grpc-server';

  save() {
    if (this.serviceList.length > 0) {
      if (!existsSync(join(this.baseDir, 'services'))) {
        mkdirSync(join(this.baseDir, 'services'));
      }
    }

    this.serviceList.forEach(f => {
      const writeServiceMethod = (method): string[] => {
        const { name, requestType, requestStream, responseType, responseStream } = method;
        const paramName = requestType !== 'void' ? requestType.charAt(0).toLowerCase() + requestType.slice(1) : '';
        const param = requestType !== 'void' ? (requestStream ? `${paramName}: ReplaySubject<${requestType}>` : `${paramName}: ${requestType}, `) : 'empty: void, ';

        const header = `  ${responseStream ? '' : 'async '}${name}(${param}metadata?: Metadata, guard?: any): ${responseStream ? `ReplaySubject<${responseType}>` : `Promise<${responseType}>`} {`;
        const content: string[] = [];

        if (responseStream) {
          content.push(`    const subject = new ReplaySubject<${responseType}>();`);
          content.push(`    subject.error(new GrpcError('UNIMPLEMENTED', status.UNIMPLEMENTED));`);
          content.push('    return subject;');
        } else {
          content.push(`    throw new GrpcError('UNIMPLEMENTED', status.UNIMPLEMENTED);`);
        }

        return ['  @GrpcMethod()']
          .concat(requestStream ? [`  @GrpcclassValidation(${requestType})`, header] : [header])
          .concat(content)
          .concat(['  }', '']);
      };

      if (!existsSync(join(this.baseDir, 'services', `${this.getFilename(f.name)}.ts`))) {
        const serviceLines = [
          `import { GrpcService, GrpcMethod, GrpcError } from '@telmoandrade/grpc-ts';`,
          `import { Metadata, status } from 'grpc';`,
          ...f.methods.some(s => s.requestStream || s.responseStream) ? [`import { ReplaySubject } from 'rxjs';`, ''] : [''],
        ]
          .concat([
            'import {',
            `  ${f.name} as ${f.name}Definition,`,
            ...f.imports.map(m => `  ${m.name},`),
            `} from '../${this.getFilename(this.protoFile)}.definition';`,
            '',
          ])
          .concat([`@GrpcService()`, `export class ${f.name} implements ${f.name}Definition {`])
          .concat(f.methods.map(method => writeServiceMethod(method)).reduce((acc, l: string[]) => [...acc, ...l], []))
          .concat(['}', '']);
        writeFileSync(join(this.baseDir, 'services', `${this.getFilename(f.name)}.ts`), serviceLines.join('\n'), 'utf8');
      }
    });

    if (!existsSync(join(this.baseDir, `${this.protoFile}.module.ts`))) {
      const moduleLines = [`import { GrpcModule } from '@telmoandrade/grpc-ts';`, `import { join } from 'path';`, '']
        .concat(this.serviceList.map(i => `import { ${i.name} } from './services/${this.getFilename(i.name)}';`))
        .concat(['', '@GrpcModule({', `  protoFile: join(__dirname, '${this.protoFile}.proto'),`, '  services: ['])
        .concat(this.serviceList.map(m => `    ${m.name},`))
        .concat(['  ],', '})', `export class ${this.protoFile.split(/[.-]/).map(x => x.charAt(0).toUpperCase() + x.slice(1)).join('')}Module {}`, '']);
      writeFileSync(join(this.baseDir, `${this.protoFile}.module.ts`), moduleLines.join('\n'), 'utf8');
    }
  }
}

const bin = new BinServer();
bin.start(process.argv.splice(2));
