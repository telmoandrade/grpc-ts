import { loadSync } from '@grpc/proto-loader';
import { loadPackageDefinition } from 'grpc';
import { existsSync, writeFileSync } from 'fs';
import { parse, join } from 'path';

import { Logger } from '../utils/logger';

export interface Enum {
  name: string;
  values: { name: string, value: number }[];
}

export interface Message {
  name: string;
  fields: { name: string, type: string }[];
}

export interface Service {
  name: string;
  imports: { name: string }[];
  methods: { name: string, requestType: string, requestStream: boolean, responseType: string, responseStream: boolean }[];
}

export abstract class Bin {
  abstract commandName: string;

  private logger = new Logger();
  protected protoFile: string;
  protected baseDir: string;
  private packageDefinition;

  private enumList: Enum[] = [];
  private messageList: Message[] = [];
  protected serviceList: Service[] = [];

  private showUsage() {
    this.logger.log('Usage:');
    this.logger.log(`   ${this.commandName} [file.proto]`);
    process.exit(1);
  }

  private loadProtofile(protoFile) {
    if (!existsSync(protoFile)) {
      this.logger.log(this.logger.red(`File not found: ${protoFile}`));
      process.exit(1);
    }

    const packageLoad = loadSync(protoFile);
    const propertyName = Object.getOwnPropertyNames(packageLoad)[0].split('.');
    propertyName.pop();
    let packageDefinition: any = loadPackageDefinition(packageLoad);
    while (propertyName.length > 0) { packageDefinition = packageDefinition[propertyName.shift()]; }
    this.packageDefinition = packageDefinition;
    this.protoFile = parse(protoFile).name;
    this.baseDir = parse(protoFile).dir;
  }

  private validateName(item) {
    if (
      this.enumList.some(s => s.name === item.name) ||
      this.messageList.some(s => s.name === item.name) ||
      this.serviceList.some(s => s.name === item.name)
    ) {
      this.logger.log(`${this.logger.red('ERROR')}: Conflict of names: the name '${this.logger.red(item.name)}' is present in more than one entity in the file '${this.protoFile}'`);
      process.exit(1);
    }
  }

  protected capitalizeFirstLetter(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  private addEnum(item) {
    this.validateName(item);

    const _enum: Enum = {
      name: item.name,
      values: item.value.map(m => {
        return {
          name: m.name.toLowerCase().split('_').reduce((acc, r) => acc + this.capitalizeFirstLetter(r), ''),
          value: m.number,
        };
      }),
    };

    this.enumList.push(_enum);
  }

  private convertFieldToType(type: string): string {
    switch (type) {
      case 'TYPE_STRING':
        return 'string';

      case 'TYPE_INT32':
      case 'TYPE_DOUBLE':
      case 'TYPE_FLOAT':
      case 'TYPE_INT64':
      case 'TYPE_UINT32':
      case 'TYPE_UINT64':
      case 'TYPE_SINT32':
      case 'TYPE_SINT64':
      case 'TYPE_FIXED32':
      case 'TYPE_FIXED64':
      case 'TYPE_SFIXED32':
      case 'TYPE_SFIXED64':
        return 'number';

      case 'TYPE_BOOL':
        return 'boolean';

      case 'TYPE_BYTES':
        return 'Buffer';

      case 'TYPE_ENUM':
        return 'enum';

      case 'TYPE_MESSAGE':
        return 'message';
    }
  }

  private convertFieldToLog(field): string {
    if (field.type === 'TYPE_ENUM' || field.type === 'TYPE_MESSAGE') {
      return field.typeName.split('.').pop();
    } else {
      return field.type.toLowerCase().split('_')[1];
    }
  }

  private addMessage(item) {
    if (item.field.length === 0) { return; }
    this.validateName(item);
    const message: Message = {
      name: item.name,
      fields: [],
    };
    item.field.map(m => {
      const name = m.name;
      const typeName = m.typeName.split('.').pop();
      let type = this.convertFieldToType(m.type);
      let isArray = m.label === 'LABEL_REPEATED';

      const mapItem = item.nestedType.find(f => (f.options || { mapEntry: false }).mapEntry && f.name === typeName);
      if (mapItem) {
        const indexType = this.convertFieldToType(mapItem.field[0].type);
        if (indexType !== 'string' && indexType !== 'number') {
          this.logger.log(`${this.logger.red('ERROR')}: Parameter '${this.logger.red(this.convertFieldToLog(mapItem.field[0]))}' dont supported!\n`);
          this.logger.log(`message ${message.name} {`);
          this.logger.log(`  map<${this.convertFieldToLog(mapItem.field[0])}, ${this.convertFieldToLog(mapItem.field[1])}> ${name} = ${m.number};`);
          this.logger.log('}');
          process.exit(1);
        }
        let indexValueType = this.convertFieldToType(mapItem.field[1].type);
        if (indexValueType === 'enum' || indexValueType === 'message') {
          indexValueType = mapItem.field[1].typeName.split('.').pop();
        }

        type = `{ [index: ${indexType}]: ${indexValueType} }`;
        isArray = false;
      }

      if (type === 'enum' || type === 'message') {
        type = typeName;
      }

      if (isArray) {
        type = `${type}[]`;
      }

      message.fields.push({ name, type });
    });

    this.messageList.push(message);

    item.enumType.forEach(f => this.addEnum(f));
    item.nestedType
      .filter(f => (f.options || { mapEntry: false }).mapEntry === false)
      .forEach(f => this.addMessage(f));
  }

  private addService(name: string, item) {
    this.validateName(item);
    const service: Service = {
      name,
      imports: [],
      methods: [],
    };

    service.methods = Object.keys(item.service).map(key => {
      const method = item.service[key];
      let requestType = method.requestType.type.name;
      let responseType = method.responseType.type.name;

      if (!this.messageList.some(s => s.name === requestType)) {
        requestType = 'void';
      } else {
        if (!service.imports.some(s => s.name === requestType)) {
          service.imports.push({ name: requestType });
        }
      }
      if (!this.messageList.some(s => s.name === responseType)) {
        responseType = 'void';
      } else {
        if (!service.imports.some(s => s.name === responseType)) {
          service.imports.push({ name: responseType });
        }
      }

      return {
        name: method.originalName,
        requestType,
        requestStream: method.requestStream,
        responseType,
        responseStream: method.responseStream,
      };
    });

    this.serviceList.push(service);
  }

  private findObjects() {
    for (const key of Object.keys(this.packageDefinition)) {
      const item = this.packageDefinition[key];
      if (typeof item !== 'function' && !item.service) {
        switch (item.format) {
          case 'Protocol Buffer 3 EnumDescriptorProto':
            this.addEnum(item.type);
            break;
          case 'Protocol Buffer 3 DescriptorProto':
            this.addMessage(item.type);
            break;
        }
      }
    }
    for (const key of Object.keys(this.packageDefinition)) {
      const item = this.packageDefinition[key];
      if (typeof item === 'function' && item.service) {
        this.addService(key, item);
      }
    }
  }

  protected getFilename(str: string) {
    return str.split(/(?=[A-Z])/).map(x => x.toLowerCase()).join('-');
  }

  private saveDefinition() {
    const lines = [
      '/**',
      ' * DOES NOT CHANGE THE CONTENTS OF THIS FILE',
      ' */',
      '',
      `import { Metadata } from 'grpc';`];
    if (this.serviceList.some(s => s.methods.some(m => m.requestStream || m.responseStream))) {
      lines.push(`import { ReplaySubject } from 'rxjs';`);
    }
    lines.push('');

    this.enumList.forEach(f => {
      lines.push(...[`export enum ${f.name} {`]
        .concat(f.values.map(value => `  ${value.name} = ${value.value},`))
        .concat(['}', '']));
    });

    this.messageList.forEach(f => {
      lines.push(...[`export interface ${f.name} {`]
        .concat(f.fields.map(field => `  ${field.name}${this.commandName === 'grpc-client' ? '?' : ''}: ${field.type};`))
        .concat(['}', '']));
    });

    const writeMethod = (method): string => {
      const { name, requestType, requestStream, responseType, responseStream } = method;
      const paramName = requestType !== 'void' ? requestType.charAt(0).toLowerCase() + requestType.slice(1) : '';
      const param = requestType !== 'void' ? (requestStream ? `${paramName}: ReplaySubject<${requestType}>` : `${paramName}: ${requestType}, `) : 'empty: void, ';
      return `  ${name}(${param}metadata?: Metadata): ${responseStream ? `ReplaySubject<${responseType}>` : `Promise<${responseType}>`};`;
    };

    this.serviceList.forEach(f => {
      lines.push(...[`export interface ${f.name} {`]
        .concat(f.methods.map(method => writeMethod(method)))
        .concat(['}', '']));
    });
    writeFileSync(join(this.baseDir, `${this.getFilename(`${this.protoFile}.definition`)}.ts`), lines.join('\n'), 'utf8');
  }

  abstract save();

  start(args: string[]) {
    if (args.length !== 1) {
      this.showUsage();
    }
    this.loadProtofile(args[0]);
    this.findObjects();
    this.saveDefinition();
    this.save();
  }
}
