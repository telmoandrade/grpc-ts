#!/usr/bin/env node
import { writeFileSync } from 'fs';
import { join } from 'path';

import { Bin } from './bin';

class BinClient extends Bin {
  commandName = 'grpc-client';

  save() {
    const lines = [
      `import { GrpcClient, Injectable } from '@telmoandrade/grpc-ts';`,
      `import { credentials } from 'grpc';`,
      `import { join } from 'path';`,
      '',
    ];

    const imports = this.serviceList.map(m => m.name).sort();
    if (imports.length > 0) {
      lines.push(...['import {']
        .concat(imports.map(i => `  ${i},`))
        .concat([`} from './${this.getFilename(`${this.protoFile}.definition`)}';`, '']));
    }

    lines.push(...['@Injectable()', `export class ${this.protoFile.split(/[.-]/).map(x => x.charAt(0).toUpperCase() + x.slice(1)).join('')}Client {`]
      .concat(this.serviceList.reduce((acc, s) => {
        return acc
          .concat([
            `  @GrpcClient<${s.name}>({`,
            `    host: process.env.GRPC, // TODO process.env.GRPC`,
            '    creadential: credentials.createInsecure(),',
            `    protoFile: join(__dirname, '${this.protoFile}.proto'),`,
            `    serviceName: '${s.name}',`,
            '    methods: {',
          ])
          .concat(s.methods.map(m => `      ${m.name}: '${this.capitalizeFirstLetter(m.name)}',`))
          .concat([
            '    },',
            '  })',
            `  ${s.name.charAt(0).toLowerCase() + s.name.slice(1)}: ${s.name};`,
            '',
          ]);
      }, []))
      .concat(['}', '']));

    writeFileSync(join(this.baseDir, `${this.getFilename(`${this.protoFile}.client`)}.ts`), lines.join('\n'), 'utf8');
  }
}

const bin = new BinClient();
bin.start(process.argv.splice(2));
