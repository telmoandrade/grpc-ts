export class Logger {
  private reference: any;
  private logger?: {
    log(message: string): void;
  };

  constructor() {
    this.logger = console;
    this.reference = {
      reset: '\x1b[0m',

      fgRed: '\x1b[31m',
      fgGreen: '\x1b[32m',
      fgYellow: '\x1b[33m',
      fgBlue: '\x1b[34m',
      fgMagenta: '\x1b[35m',
      fgCyan: '\x1b[36m',
      fgWhite: '\x1b[37m',
    };
  }

  private color(text: string, color: string) {
    return `${this.reference[color]}${text}${this.reference.reset}`;
  }

  red(text: string): string { return this.color(text, 'fgRed'); }
  green(text: string): string { return this.color(text, 'fgGreen'); }
  yellow(text: string): string { return this.color(text, 'fgYellow'); }
  blue(text: string): string { return this.color(text, 'fgBlue'); }
  magenta(text: string): string { return this.color(text, 'fgMagenta'); }
  cyan(text: string): string { return this.color(text, 'fgCyan'); }
  white(text: string): string { return this.color(text, 'fgWhite'); }

  log(message: string) {
    this.logger.log(message);
  }
}
