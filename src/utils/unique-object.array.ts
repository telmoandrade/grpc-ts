export class UniqueObjectArray<T extends object> {
  private list: T[] = [];

  push(obj: T) {
    if (!this.list.some(x => this.equals(x, obj))){
      this.list.push(obj);
    }
  }

  get(): T[] {
    return this.list;
  }

  private equals(c1: T, c2: T): boolean {
    const keys1 = Object.getOwnPropertyNames(c1);
    for (const key of keys1) {
      const v1 = c1[key];
      const v2 = c2[key];

      if (v1 !== v2) {
        if (typeof v1 === 'object' && typeof v2 === 'object') {
          if (!this.equals(v1, v2)) {
            return false;
          }
        } else {
          return false;
        }
      }
    }

    const keys2 = Object.getOwnPropertyNames(c1);
    for (const key of keys2) {
      const v1 = c1[key];
      const v2 = c2[key];

      if (v1 !== v2) {
        if (typeof v1 === 'object' && typeof v2 === 'object') {
          if (!this.equals(v1, v2)) {
            return false;
          }
        } else {
          return false;
        }
      }
    }

    return true;
  }
}
