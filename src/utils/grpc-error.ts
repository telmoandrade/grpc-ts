import { ValidationError } from 'class-validator';
import { status } from 'grpc';

export class GrpcError extends Error {
  public code: number;
  public details?: string;
  public innerError: Error;
  public invalidArgument: ValidationError[];

  constructor(message: string, code: status, invalidArgument?: ValidationError[]) {
    super(message);
    this.code = code;
    this.invalidArgument = invalidArgument;
  }
}
